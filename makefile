.PRECIOUS: %.cmi %.cmo

compile : tp4.ml image.cmo
	ocamlc -o tp4 graphics.cma image.cmo tp4.ml  

run : compile
	./tp4

%.cmi : %.mli
	ocamlc -c $^

%.cmo : %.cmi %.ml
	ocamlc -c $(word 2,$^)

clean :
	rm -rf *.cmi
	rm -rf *.cmo
	rm -rf tp4
