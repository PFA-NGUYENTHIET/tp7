open Graphics
open Image

type image_tree = color t

(* Exemple du TP. *)
let a0 = N (N (F white,F black,F black,F black),F black,F white,F black)

let t0 = [|
    [| white ; black ; white ; white |] ;
    [| black ; black ; white ; white |] ;
    [| black ; black ; black ; black |] ;
    [| black ; black ; black ; black |]
  |]

(* Question 1.1 *)
let rec get_pixel x y longueur arbre =
    let middle = longueur / 2 in
    match arbre with
    | F(e) -> e
    | N(so,se,no,ne) -> 
            if x >= middle then 
                if y >= middle then
                    get_pixel (x-middle) (y-middle) middle ne
                else
                    get_pixel (x-middle) y middle se
            else
                if y >= middle then
                    get_pixel x (y-middle) middle no
                else
                    get_pixel x y middle so

(* Question 1.2 *)
let image_matrix_of_tree longueur arbre = 
        Array.init longueur (fun i -> Array.init longueur (fun j -> get_pixel i j longueur arbre ))

let () =
  (*affiche a0;
  ignore(Graphics.read_key())*)
    let c = get_pixel 1 2 4 a0 in
    Printf.printf "%B" ( white = c);

    let t = image_matrix_of_tree 4 a0 in
    Printf.printf "%B" (t = t0)

